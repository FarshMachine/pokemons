import React, { useEffect } from 'react'
import { useRouteMatch, Link } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import isEmpty from 'lodash/isEmpty'
import upperFirst from 'lodash/upperFirst'
import chooseEngLang from './kit'
import {
  clearAbility,
  fetchAndSetAbilityData,
  useAbilityIsFetching,
  useAbilityData,
  useAbilityErrorMsg,
} from '../../store/branches/ability'

const NO_DATA = 'No data'

const AbilityInfo = () => {
  const dispatch = useDispatch()
  const isFetching = useAbilityIsFetching()
  const data = useAbilityData()
  const fetchErrorMsg = useAbilityErrorMsg()
  const entries = chooseEngLang(data.effect_entries)

  const {
    params: { pokemonId, abilityId },
  } = useRouteMatch()

  useEffect(() => {
    dispatch(fetchAndSetAbilityData(abilityId))

    return () => dispatch(clearAbility())
  }, [abilityId, dispatch])

  if (fetchErrorMsg) {
    return <div className="fetch-error">{fetchErrorMsg}</div>
  }

  if (isFetching) {
    return <div className="loading">Loading...</div>
  }

  if (isEmpty(data)) {
    return null
  }

  return (
    <>
      <Link to={`/pokemon/${pokemonId}`} className="back-button">
        To pokemon
      </Link>
      <div className="ability-info-page">
        <div className="name item">
          <b>Name:</b> {upperFirst(data.name)}
        </div>
        <div className="effect item">
          <b>Effect:</b> {entries.effect || NO_DATA}
        </div>
        <div className="short_effect item">
          <b>Short effect:</b> {entries.short_effect || NO_DATA}
        </div>
      </div>
    </>
  )
}

export default React.memo(AbilityInfo)
