const ENGLISH_LANGUAGE = 'en'

export default (entries) => {
  if (!entries) return null

  const filteredEntries = entries.filter(
    (entry) => entry.language.name === ENGLISH_LANGUAGE
  )

  return filteredEntries[0] || entries[0]
}
