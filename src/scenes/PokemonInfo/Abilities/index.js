import React from 'react'
import propTypes from 'prop-types'
import upperFirst from 'lodash/upperFirst'
import { Link, useLocation } from 'react-router-dom'

const Abilities = ({ abilities }) => {
  const { pathname } = useLocation()

  return (
    <div className="info-item">
      <div>Abilities:</div>
      <div className="abilities">
        {abilities.map(({ ability }) => {
          const abilityUrlArr = ability.url.split('/')
          const abilityId = abilityUrlArr[abilityUrlArr.length - 2]

          return (
            <Link
              key={abilityId}
              to={`${pathname}/ability/${abilityId}`}
              className="ability"
            >
              {upperFirst(ability.name)}
            </Link>
          )
        })}
      </div>
    </div>
  )
}

Abilities.propTypes = {
  abilities: propTypes.arrayOf(propTypes.shape),
}

Abilities.defaultProps = {
  abilities: [],
}

export default React.memo(Abilities)
