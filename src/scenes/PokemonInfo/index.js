import React, { useEffect } from 'react'
import { useRouteMatch, Link } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import isEmpty from 'lodash/isEmpty'
import {
  clearPokemon,
  fetchAndSetPokemonData,
  usePokemonIsFetching,
  usePokemonData,
  usePokemonErrorMsg,
} from '../../store/branches/pokemon'
import Name from './Name'
import Types from './Types'
import Abilities from './Abilities'
import Stats from './Stats'

const PokemonInfo = () => {
  const dispatch = useDispatch()
  const isFetching = usePokemonIsFetching()
  const data = usePokemonData()
  const fetchErrorMsg = usePokemonErrorMsg()

  const {
    params: { pokemonId },
  } = useRouteMatch()

  useEffect(() => {
    dispatch(fetchAndSetPokemonData(pokemonId))

    return () => dispatch(clearPokemon())
  }, [dispatch, pokemonId])

  if (fetchErrorMsg) {
    return <div className="fetch-error">{fetchErrorMsg}</div>
  }

  if (isFetching) {
    return <div className="loading">Loading...</div>
  }

  if (isEmpty(data)) {
    return null
  }

  return (
    <>
      <Link to="/" className="back-button">
        To main
      </Link>
      <div className="pokemon-info-page">
        <img
          src={`https://pokeres.bastionbot.org/images/pokemon/${pokemonId}.png`}
          alt="Pokemon"
        />
        <div className="info">
          <Name name={data.name} />
          <Types types={data.types} />
          <Abilities abilities={data.abilities} />
        </div>
        <Stats stats={data.stats} />
      </div>
    </>
  )
}

export default React.memo(PokemonInfo)
