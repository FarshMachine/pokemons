import React from 'react'
import propTypes from 'prop-types'

const Types = ({ types }) => {
  return (
    <div className="info-item">
      <div>Types:</div>
      <div className="types">
        {types.map(({ type }) => {
          return (
            <div key={type.name} className={`type ${type.name}`}>
              {type.name}
            </div>
          )
        })}
      </div>
    </div>
  )
}

Types.propTypes = {
  types: propTypes.arrayOf(propTypes.shape),
}

Types.defaultProps = {
  types: [],
}

export default React.memo(Types)
