import React from 'react'
import propTypes from 'prop-types'
import upperFirst from 'lodash/upperFirst'

const Name = ({ name }) => {
  return (
    <div className="info-item">
      <div>Name:</div>
      <div className="name">{upperFirst(name)}</div>
    </div>
  )
}

Name.propTypes = {
  name: propTypes.string,
}

Name.defaultProps = {
  name: '',
}

export default React.memo(Name)
