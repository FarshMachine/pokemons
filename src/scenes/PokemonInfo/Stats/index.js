import React from 'react'
import propTypes from 'prop-types'

const Stats = ({ stats }) => {
  return (
    <div className="stats">
      {stats.map((stat) => {
        return (
          <div key={stat.stat.name} className="stat">
            <div className="name">{stat.stat.name}</div>
            <div className="value">
              <div className="max-value-line">
                <div className="value-text">{`${stat.base_stat} / 200`}</div>
                <div
                  className="curr-value-line"
                  style={{ width: `${stat.base_stat / 2}%` }}
                />
              </div>
            </div>
          </div>
        )
      })}
    </div>
  )
}

Stats.propTypes = {
  stats: propTypes.arrayOf(propTypes.shape),
}

Stats.defaultProps = {
  stats: [],
}

export default React.memo(Stats)
