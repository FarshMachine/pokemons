import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import debounce from 'lodash/debounce'
import {
  fetchAndSetPokemonsData,
  clearPokemons,
} from '../../store/branches/pokemons'
import PokemonList from './PokemonList'

const debouncedFetchAndSetPokemonsData = debounce((dispatch, searchString) => {
  dispatch(fetchAndSetPokemonsData(searchString))
}, 300)

const Main = () => {
  const [searchString, setSearchString] = useState('')
  const dispatch = useDispatch()

  useEffect(() => {
    debouncedFetchAndSetPokemonsData(dispatch, searchString)
  }, [dispatch, searchString])

  useEffect(() => {
    return () => dispatch(clearPokemons())
  }, [dispatch])

  const onChangeHandler = (e) => {
    setSearchString(e.target.value)
  }

  return (
    <div className="pokemons-page">
      <div className="search-input">
        <div className="search-input-image" />
        <input
          value={searchString}
          onChange={onChangeHandler}
          placeholder="Enter the name of the pokemon..."
        />
        <div className="search-input-image" />
      </div>
      <PokemonList />
    </div>
  )
}

export default React.memo(Main)
