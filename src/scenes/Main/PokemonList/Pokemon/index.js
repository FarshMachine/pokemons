import React from 'react'
import PropTypes from 'prop-types'
import { useHistory } from 'react-router-dom'

const Pokemon = ({ id, name, url }) => {
  const history = useHistory()

  const onClickHandler = () => {
    history.push(`/pokemon/${id}`)
  }

  return (
    <div
      className="pokemon-card"
      onClick={onClickHandler}
      onKeyDown={() => {}}
      role="link"
      tabIndex={-1}
    >
      <img src={url} alt={name} />
      <div className="name">{name}</div>
    </div>
  )
}

Pokemon.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
}

export default React.memo(Pokemon)
