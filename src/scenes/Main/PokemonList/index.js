import React from 'react'
import isEmpty from 'lodash/isEmpty'
import Pokemon from './Pokemon'
import {
  usePokemonsIsFetching,
  usePokemonsData,
  usePokemonsErrorMsg,
} from '../../../store/branches/pokemons'

const PokemonList = () => {
  const isFetching = usePokemonsIsFetching()
  const data = usePokemonsData()
  const fetchErrorMsg = usePokemonsErrorMsg()

  if (fetchErrorMsg) {
    return <div className="fetch-error">{fetchErrorMsg}</div>
  }

  if (isFetching) {
    return <div className="loading">Loading...</div>
  }

  if (isEmpty(data)) {
    return null
  }

  return (
    <div className="pokemons-list">
      {data.map(({ name, url }) => {
        const urlArr = url.split('/')
        const pokemonId = urlArr[urlArr.length - 2]
        const imageUrl = `https://pokeres.bastionbot.org/images/pokemon/${pokemonId}.png`

        return (
          <Pokemon key={pokemonId} id={pokemonId} url={imageUrl} name={name} />
        )
      })}
    </div>
  )
}

export default React.memo(PokemonList)
