import { applyMiddleware, createStore, combineReducers } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import pokemonsReducer from './branches/pokemons'
import pokemonReducer from './branches/pokemon'
import abilityReducer from './branches/ability'

const reducer = combineReducers({
  pokemons: pokemonsReducer,
  pokemon: pokemonReducer,
  ability: abilityReducer,
})

export default createStore(reducer, composeWithDevTools(applyMiddleware(thunk)))
