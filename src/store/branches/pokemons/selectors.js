import { useSelector } from 'react-redux'

const getPokemonsIsFetching = (state) => state.pokemons.isFetching
const getPokemonsData = (state) => state.pokemons.data
const getPokemonsErrorMsg = (state) => state.pokemons.errorMsg

export const usePokemonsIsFetching = () => {
  return useSelector(getPokemonsIsFetching)
}

export const usePokemonsData = () => {
  return useSelector(getPokemonsData)
}

export const usePokemonsErrorMsg = () => {
  return useSelector(getPokemonsErrorMsg)
}
