import { handleActions } from 'redux-actions'
import {
  setPokemonsData,
  setPokemonsIsFetching,
  setPokemonsFetchErrorMsg,
  clearPokemons,
} from './actions'

const initialState = {
  isFetching: false,
  data: [],
  fetchErrorMsg: '',
}

export default handleActions(
  {
    [setPokemonsData]: (state, { payload }) => {
      return {
        ...state,
        data: payload,
      }
    },
    [setPokemonsIsFetching]: (state, { payload }) => {
      return {
        ...state,
        isFetching: payload,
      }
    },
    [setPokemonsFetchErrorMsg]: (state, { payload }) => {
      return {
        ...state,
        fetchErrorMsg: payload,
      }
    },
    [clearPokemons]: (state) => {
      return {
        ...state,
        ...initialState,
      }
    },
  },
  initialState
)
