import { createActions } from 'redux-actions'
import axios from 'axios'

export const {
  setPokemonsData,
  setPokemonsIsFetching,
  setPokemonsFetchErrorMsg,
  clearPokemons,
} = createActions(
  'SET_POKEMONS_IS_FETCHING',
  'SET_POKEMONS_DATA',
  'SET_POKEMONS_FETCH_ERROR_MSG',
  'CLEAR_POKEMONS'
)

export const fetchAndSetPokemonsData = (searchString = '') => async (
  dispatch
) => {
  dispatch(setPokemonsFetchErrorMsg(''))

  try {
    dispatch(setPokemonsIsFetching(true))

    const {
      data: { results },
    } = await axios.get('https://pokeapi.co/api/v2/pokemon?offset=0&limit=30')

    const pokemons = results.filter(
      ({ name }) => name.includes(searchString.toLowerCase()) || !searchString
    )

    dispatch(setPokemonsData(pokemons))
    dispatch(setPokemonsIsFetching(false))
  } catch {
    dispatch(setPokemonsFetchErrorMsg('Fetch Error'))
    dispatch(setPokemonsIsFetching(false))
  }
}
