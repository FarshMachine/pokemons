import { handleActions } from 'redux-actions'
import {
  setPokemonData,
  setPokemonIsFetching,
  setPokemonFetchErrorMsg,
  clearPokemon,
} from './actions'

const initialState = {
  isFetching: false,
  data: {},
  fetchErrorMsg: '',
}

export default handleActions(
  {
    [setPokemonData]: (state, { payload }) => {
      return {
        ...state,
        data: payload,
      }
    },
    [setPokemonIsFetching]: (state, { payload }) => {
      return {
        ...state,
        isFetching: payload,
      }
    },
    [setPokemonFetchErrorMsg]: (state, { payload }) => {
      return {
        ...state,
        fetchErrorMsg: payload,
      }
    },
    [clearPokemon]: (state) => {
      return {
        ...state,
        ...initialState,
      }
    },
  },
  initialState
)
