import { useSelector } from 'react-redux'

const getPokemonIsFetching = (state) => state.pokemon.isFetching
const getPokemonData = (state) => state.pokemon.data
const getPokemonErrorMsg = (state) => state.pokemon.errorMsg

export const usePokemonIsFetching = () => {
  return useSelector(getPokemonIsFetching)
}

export const usePokemonData = () => {
  return useSelector(getPokemonData)
}

export const usePokemonErrorMsg = () => {
  return useSelector(getPokemonErrorMsg)
}
