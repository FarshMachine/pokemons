import { createActions } from 'redux-actions'
import axios from 'axios'

export const {
  setPokemonData,
  setPokemonIsFetching,
  setPokemonFetchErrorMsg,
  clearPokemon,
} = createActions(
  'SET_POKEMON_IS_FETCHING',
  'SET_POKEMON_DATA',
  'SET_POKEMON_FETCH_ERROR_MSG',
  'CLEAR_POKEMON'
)

export const fetchAndSetPokemonData = (pokemonId) => async (dispatch) => {
  dispatch(setPokemonFetchErrorMsg(''))

  try {
    dispatch(setPokemonIsFetching(true))

    const { data } = await axios.get(
      `https://pokeapi.co/api/v2/pokemon/${pokemonId}`
    )

    dispatch(setPokemonData(data))
    dispatch(setPokemonIsFetching(false))
  } catch {
    dispatch(setPokemonFetchErrorMsg('Fetch Error'))
    dispatch(setPokemonIsFetching(false))
  }
}
