import { handleActions } from 'redux-actions'
import {
  setAbilityData,
  setAbilityIsFetching,
  setAbilityFetchErrorMsg,
  clearAbility,
} from './actions'

const initialState = {
  isFetching: false,
  data: {},
  fetchErrorMsg: '',
}

export default handleActions(
  {
    [setAbilityData]: (state, { payload }) => {
      return {
        ...state,
        data: payload,
      }
    },
    [setAbilityIsFetching]: (state, { payload }) => {
      return {
        ...state,
        isFetching: payload,
      }
    },
    [setAbilityFetchErrorMsg]: (state, { payload }) => {
      return {
        ...state,
        fetchErrorMsg: payload,
      }
    },
    [clearAbility]: (state) => {
      return {
        ...state,
        ...initialState,
      }
    },
  },
  initialState
)
