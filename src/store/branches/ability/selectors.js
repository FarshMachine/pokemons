import { useSelector } from 'react-redux'

const getAbilityIsFetching = (state) => state.ability.isFetching
const getAbilityData = (state) => state.ability.data
const getAbilityErrorMsg = (state) => state.ability.errorMsg

export const useAbilityIsFetching = () => {
  return useSelector(getAbilityIsFetching)
}

export const useAbilityData = () => {
  return useSelector(getAbilityData)
}

export const useAbilityErrorMsg = () => {
  return useSelector(getAbilityErrorMsg)
}
