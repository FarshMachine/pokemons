import { createActions } from 'redux-actions'
import axios from 'axios'

export const {
  setAbilityData,
  setAbilityIsFetching,
  setAbilityFetchErrorMsg,
  clearAbility,
} = createActions(
  'SET_ABILITY_IS_FETCHING',
  'SET_ABILITY_DATA',
  'SET_ABILITY_FETCH_ERROR_MSG',
  'CLEAR_ABILITY'
)

export const fetchAndSetAbilityData = (abilityId) => async (dispatch) => {
  dispatch(setAbilityFetchErrorMsg(''))

  try {
    dispatch(setAbilityIsFetching(true))

    const { data } = await axios.get(
      `https://pokeapi.co/api/v2/ability/${abilityId}`
    )

    dispatch(setAbilityData(data))
    dispatch(setAbilityIsFetching(false))
  } catch {
    dispatch(setAbilityFetchErrorMsg('Fetch Error'))
    dispatch(setAbilityIsFetching(false))
  }
}
