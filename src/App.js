import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { hot } from 'react-hot-loader/root'
import Main from './scenes/Main'
import PokemonInfo from './scenes/PokemonInfo'
import AbilityInfo from './scenes/AbilityInfo'

const App = () => {
  return (
    <>
      <Switch>
        <Route exact path="/" component={Main} />
        <Route exact path="/pokemon/:pokemonId" component={PokemonInfo} />
        <Route
          path="/pokemon/:pokemonId/ability/:abilityId"
          component={AbilityInfo}
        />
      </Switch>
    </>
  )
}

export default hot(App)
